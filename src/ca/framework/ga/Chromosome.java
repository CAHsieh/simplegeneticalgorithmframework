package ca.framework.ga;

import java.util.Arrays;
import java.util.Random;

/**
 * 基因演算法的染色體部分，
 * 實作包含交配、突變等方法，
 * 以布林陣列作為染色體本體。
 * 詳細的染色體內容(ex.要開多長、分為多少區間、屬性獲取)
 * 需根據特定需求自行增加。
 *
 * @author CAHsieh
 */
public class Chromosome {

    /**
     * 染色體總長度
     */
    private int ChromosomeLength;

    /**
     * 染色體主體
     */
    private boolean[] chromosome;

    /**
     * 用於儲存此染色體的配適度，
     * 根據情況自行決定配適度為何。
     */
    private double Fitness;

    /**
     * default constructor
     * 請自行依據需求修改
     */
    public Chromosome() {
        ChromosomeLength = 10;
        chromosome = new boolean[ChromosomeLength];
        Fitness = 0;

        // 初始時隨機產生染色體
        Random random = new Random();
        for (int i = 0; i < ChromosomeLength; i++) {
            chromosome[i] = random.nextBoolean();
        }
    }

    /**
     * 用於交配的建構子
     *
     * @param chr1       第一條染色體的布林陣列(染色體本體)
     * @param chr2       第二條染色體的布林陣列(染色體本體)
     * @param cross_rate 交配率
     */
    private Chromosome(boolean[] chr1, boolean[] chr2, double cross_rate) {
        ChromosomeLength = chr1.length;
        chromosome = new boolean[ChromosomeLength];
        Random random = new Random();

        int chr1Num = (int) (ChromosomeLength * cross_rate); //根據交配率來決定從chr1來的基因數量

        for (int i = 0; i < chr1Num; i++) {

            int idx;
            do {
                idx = random.nextInt(ChromosomeLength);
            } while (chromosome[idx]); //若該位置已被挑選過則重新挑選。

            chromosome[idx] = true; // 先將選中的index標記為true，表示此位置的基因來自chr1。
        }

        for (int i = 0; i < ChromosomeLength; i++) {
            chromosome[i] = chromosome[i] ? chr1[i] : chr2[i]; //若該位置為true,取chr1的值;否則取chr2的值。
        }
    }

    /**
     * 進行交配
     *
     * @param chromosome2 第二條染色體
     * @param cross_rate  交配率
     * @return 交配後的新染色體
     */
    public Chromosome crossover(Chromosome chromosome2, double cross_rate) {
        return new Chromosome(chromosome, chromosome2.getChromosome(), cross_rate);
    }

    /**
     * 進行突變
     *
     * @param mutation_rate 突變率
     * @return 突變後的新染色體
     */
    public Chromosome mutate(double mutation_rate) {
        Chromosome newChr = new Chromosome();
        boolean[] newChromo = Arrays.copyOf(chromosome, ChromosomeLength);

        Random random = new Random();
        int MutationNum = (int) (ChromosomeLength * mutation_rate); //根據突變率計算需突變的基因個數
        boolean[] isUsed = new boolean[ChromosomeLength]; //用於標記已用過的染色體位置

        for (int i = 0; i < MutationNum; i++) {
            int index;
            do {
                index = random.nextInt(ChromosomeLength);
            } while (isUsed[index]); //若該位置已被挑選過則重新挑選。

            newChromo[index] = !newChromo[index]; //對該位置進行突變
            isUsed[index] = true;
        }

        newChr.setChromosome(newChromo); //將突變後的染色體設到新建構的chromosome中

        return newChr;
    }

    private void setChromosome(boolean[] chromosome) {
        this.chromosome = chromosome;
    }

    private boolean[] getChromosome() {
        return chromosome;
    }

    public void setFitness(double fitness) {
        Fitness = fitness;
    }

    public double getFitness() {
        return Fitness;
    }

//    ...
//    其餘相關參數的計算、取得須根據情況另行設計。

}
