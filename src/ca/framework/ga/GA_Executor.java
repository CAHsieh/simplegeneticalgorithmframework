package ca.framework.ga;

import java.util.Arrays;
import java.util.Random;

/**
 * 用於執行基因演算法的主類別。
 * 各參數如染色體數量、終止條件、配適度函數須根據需求另行設計。
 *
 * @author CAHsieh
 */
public class GA_Executor {

    public static void main(String[] args){
        GA_Executor executor = new GA_Executor();
        executor.run();
    }

    /**
     * 每輪保留的染色體數量。
     */
    private static final int CHR_AMOUNT =10;

    /**
     * 每次執行時的最高迭代次數。
     */
    private static final int ITERATE_TIMES = 100000;

    /**
     * 局部最佳重複次數。
     */
    private static final int OPTIMAL_TIMES = 20000;

    /**
     * Good Enough條件，
     * 須根據不同的配適度函數做調整。
     */
    private static final double GOOD_ENOUGH = 0.9;

    /**
     * 每一輪selection時所用的隨機數量。
     * 隨機挑選的數量一定要小於總保留數量（CHR_AMOUNT），否則沒意義或會出錯。
     */
    private static final int RANDOM_NUM = 0;

    private void run(){

        double last_fitness = 0; //用來記錄前一次的最佳配適度。

        Chromosome[] chromosomes = inital(); //初始產生染色體。
        fitness(chromosomes); // 先計算初始染色體的配適度結果。

        for (int times = 0, sametimes = 0; times <= ITERATE_TIMES && last_fitness < GOOD_ENOUGH && sametimes <= OPTIMAL_TIMES; times++) {

            Chromosome[] crossoverChrs = crossover(chromosomes); //進行染色體交配
            Chromosome[] mutationChrs = mutate(chromosomes); //進行染色體突變

            //計算所有染色體的配適度
            fitness(crossoverChrs);
            fitness(mutationChrs);


            chromosomes = selection(chromosomes,crossoverChrs,mutationChrs,RANDOM_NUM); //擇優保留下一代染色體

            if (last_fitness == chromosomes[0].getFitness()) sametimes++; //若本次結果的最佳配適度與前次相同，則將sametimes+1。
            else {
                //若本次結果的最佳配適度與前次不同，則修改last_fitness並將sametimes歸零。
                last_fitness = chromosomes[0].getFitness();
                sametimes = 0;
            }
        }

    }

    /**
     * 根據參數產生第一代染色體
     * @return 第一代染色體
     */
    private Chromosome[] inital(){
        Chromosome[] chromosomes = new Chromosome[CHR_AMOUNT];
        for(int i = 0; i< CHR_AMOUNT; i++){
            chromosomes[i] = new Chromosome();
        }
        return chromosomes;
    }

    /**
     * 產生交配後的染色體群
     * 隨機挑選兩條染色體進行交配
     *
     * @return 交配後的染色體群
     */
    private Chromosome[] crossover(Chromosome[] chromosomes){
        Random random = new Random();
        Chromosome[] crossoverChromosomes = new Chromosome[CHR_AMOUNT];

        //總共要產生CHR_AMOUNT條交配後的染色體
        for (int i = 0; i < CHR_AMOUNT; i++) {
            int first = random.nextInt(CHR_AMOUNT); //先隨機挑選第一條
            int second;
            do {
                second = random.nextInt(CHR_AMOUNT);
            } while (first == second); //隨機挑選的第二條不可跟第一條重複

            double cross_rate = (double) random.nextInt(10000) / 10000; //每次交配均隨機產生一次交配率。
            crossoverChromosomes[i] = chromosomes[first].crossover(chromosomes[second], cross_rate);
        }
        return crossoverChromosomes;
    }

    /**
     * 產生突變後的染色體群
     * 每一條染色體均進行一次突變
     *
     * @param chromosomes 原染色體群
     * @return 突變後的染色體群
     */
    private Chromosome[] mutate(Chromosome[] chromosomes) {
        Chromosome[] mutationChromosomes = new Chromosome[CHR_AMOUNT];
        Random random = new Random();

        //對每一條染色體進行突變
        for (int i = 0; i < CHR_AMOUNT; i++) {
            double mutate_rate = (double) random.nextInt(10000) / 10000; //每次突變均隨機產生一次突變率。
            mutationChromosomes[i] = chromosomes[i].mutate(mutate_rate);
        }
        return mutationChromosomes;
    }

    /**
     * 計算每條染色體的配適度
     * 須根據不同的情況、需求另行設計。
     *
     * @param chromosomes 要計算配適度的染色體群
     */
    private void fitness(Chromosome[] chromosomes){
        for(Chromosome chromosome:chromosomes){
            // ...
            // ...
            // 根據情況、需求，自行設計配適度函數
            chromosome.setFitness(0.9);
        }
    }


    /**
     * 擇優保留下一代
     * 隨機挑選的數量一定要小於總保留數量，否則沒意義或會出錯。
     *
     * @param origin    初始染色體群
     * @param crossover 交配後的染色體群
     * @param mutation  突變後的染色體群
     * @param random_num 隨機挑選的數量（用於避免過早陷入局部最佳）
     *
     * @return 進入下一代的染色體
     */
    private Chromosome[] selection(Chromosome[] origin, Chromosome[] crossover, Chromosome[] mutation, int random_num) {

        assert random_num < CHR_AMOUNT; //隨機挑選的數量一定要小於總保留數量，否則沒意義或會出錯。

        //先將三群併在一起
        Chromosome[] mergeArray = new Chromosome[CHR_AMOUNT * 3];
        System.arraycopy(origin, 0, mergeArray, 0, origin.length);
        System.arraycopy(crossover, 0, mergeArray, origin.length, crossover.length);
        System.arraycopy(mutation, 0, mergeArray, origin.length + crossover.length, mutation.length);

        //依據配適度進行排序，這邊的排序方法是配適度高的在前面。
        Arrays.sort(mergeArray, (o1, o2) -> {
            if (o1.getFitness() > o2.getFitness())
                return -1;
            else if (o1.getFitness() == o2.getFitness()) {
                return 0;
            } else
                return 1;
        });


        // 先挑選前面數條較高配適度的染色體
        Chromosome[] result = new Chromosome[CHR_AMOUNT];
        System.arraycopy(mergeArray, 0, result, 0, CHR_AMOUNT - random_num);


        //再根據參數挑選數條隨機的染色體
        Random random = new Random();
        int firstR = -1;
        for (int i = 0; i < random_num; i++) {
            int index = random.nextInt(CHR_AMOUNT * 3 - (CHR_AMOUNT - random_num)) + (CHR_AMOUNT - random_num);
            while (index == firstR) {
                index = random.nextInt(CHR_AMOUNT * 3 - (CHR_AMOUNT - random_num)) + (CHR_AMOUNT - random_num);
            }

            result[CHR_AMOUNT - random_num + i] = mergeArray[index];
            firstR = index;
        }

        return result;
    }
}
